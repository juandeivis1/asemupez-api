import * as express from 'express';

export const expressOptions = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    // res.setHeader("Access-Control-Allow-Origin", "https://jarvislico.ninja");
    // res.setHeader("Access-Control-Allow-Origin", "http://localhost:4200");
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Methods", "POST");
    res.setHeader("Access-Control-Allow-Headers", "Content-Type,Authorization,Geolocation");
    res.setHeader("X-Powered-By", "jarvisdev.ninja");
    // res.setHeader("Access-Control-Allow-Credentials", 'true');      
    if (req.method == 'OPTIONS') {
      res.sendStatus(200);
    } else {
      next();
    }
  };

  export const originValidation = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (req.headers.origin){
      let origin =  req.headers.origin;
      if (origin == 'https://jarvislico.ninja' || origin == 'http://localhost:4200') {
        next()    
      } else {
        res.send('<h1 style="font-family: monospace;color: cornflowerblue;margin-top: 45%;text-align: center;">You need a authoriuzation token to be here!! BYE :)</h1>')
      }
    } else {
      res.send(`<h1 style="font-family: monospace;color: #dc3545;margin-top: 9rem;text-align: center;font-size: 6.15rem;">ASEMUPEZ Server, you don't have to be here!</h1><div  style="display: block;margin-left: auto;margin-right: auto;width: 50%;"><img src="https://licojarvis.firebaseapp.com/assets/smile.png"></div>`)
    }
  }