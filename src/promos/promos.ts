import * as admin from "firebase-admin";
// import { adminFCM } from "..";

export const registrarPromo = async (request, response) => {
    const { token, promo } = request.body
    try {
        let notification = {
            notification:
            {
                title: 'Bienvenido a la familia de promos... :)',
                body: 'Dentro de las proximas minutos te enviaremos un cupón de promo!!!',
                clickAction: 'http://localhost:4200/account',
                icon: '/assets/ninja.png'
            },
            data: { route: '/account' }
        }
        let respuesta = await admin.messaging().subscribeToTopic(token, promo)
        let res = await admin.messaging()
            .sendToDevice(token, notification)
        response.json({ tokenRegister: res });
    }
    catch (err) {
        response.json({ error: 'Invalid token' })
    }
}

export const enviarPromo = async (request, response) => {
    try {
        let notification = {
            notification:
            {
                title: 'Hay nueva carga de Cuba Libre... :)',
                body: 'Podes hacer tu pedido y retirar donde definamos!!!',
                // clickAction: 'http://localhost:4200/account',
                icon: '/assets/ninja.png'
            },
            data: { route: '/account' }
        }
        // let respuesta = await admin.messaging().subscribeToTopic(token, promo)
        let res = await admin.messaging()
            .sendToTopic('promo', notification)
        response.json({ promo: res });
    }
    catch (err) {
        response.json({ error: 'Invalid token' })
    }
}

export const enviarMensaje = async (request, response) => {
    let { body, title, img, token } = request.body

    let res = await admin.messaging().sendToDevice(token, { notification: {title: title, body: body, icon: img}})

    response.json(res)
    
}