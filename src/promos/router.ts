
import * as express from 'express'
import { registrarPromo, enviarPromo } from './promos';

export const routerUser = express.Router()

routerUser.post('/register', registrarPromo)
routerUser.get('/promo', enviarPromo)