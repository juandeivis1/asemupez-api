import * as admin from "firebase-admin";
import { UserResolvers, UserDefs } from "./models/user/schema";
const { ApolloServer, AuthorizationError, makeExecutableSchema } = require('apollo-server');
import { merge } from 'lodash';
import { PrestamoResolvers, PrestamoDefs } from "./models/prestam/schema";
import { TransactionDefs, TransactionResolvers } from "./models/transaction/schema";
import { ProductoDefs, ProductoResolvers } from "./models/producto/schema";
import { DeduccionDefs, DeduccionResolvers } from "./models/deduccion/schema";
import { MovimientoDefs, MovimientoResolvers } from "./models/movimiento/schema";
import { CreditoDefs, CreditoResolvers } from "./models/credito/schema";
import { MovimientoCreditoDefs, MovimientoCreditoResolvers } from "./models/movimientoCredito/schema";
import { LineaDefs, LineaResolvers } from "./models/linea/schema";

var serviceAccount = require("./asemupez-uned-firebase-adminsdk-6ywxp-9c63b897ac.json");

export const adminFCM = admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://asemupez-uned.firebaseio.com"
});

const schema = makeExecutableSchema({
  typeDefs: [
    DeduccionDefs,
    ProductoDefs,
    UserDefs,
    PrestamoDefs,
    TransactionDefs,
    MovimientoDefs,
    CreditoDefs,
    MovimientoCreditoDefs,
    LineaDefs
  ],
  resolvers: merge(
    DeduccionResolvers,
    ProductoResolvers,
    UserResolvers,
    PrestamoResolvers,
    TransactionResolvers,
    MovimientoResolvers,
    CreditoResolvers,
    MovimientoCreditoResolvers,
    LineaResolvers
  ),
});

const server = new ApolloServer({
  schema,
  context: async ({ req }) => {

    const token = req.headers.authorization.replace('Bearer ', '') || '';

    if (!token) {
      throw new AuthorizationError('you must be logged in');
    }
    try {      
      let user = await admin.auth().verifyIdToken(token);

      return { ...user };
    }
    catch (err) {
      throw new AuthorizationError('you must be logged in');
    }

  },
});

server.listen().then(({ url }) => {
  console.log(url);
});