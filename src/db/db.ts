import * as knex from "knex";
import * as bookshelf from "bookshelf";

const knexASEMPUEZ = knex({
  client: 'mssql',
  connection: {
    host: '192.168.1.127',
    user: 'jarvis',
    password: '12345678',
    database: 'asemupez',
    charset: 'utf8',
    options: {
      encrypt: false
    }
  }
});

const ASEMPUEZ = bookshelf(knexASEMPUEZ);

export { knexASEMPUEZ as knex, ASEMPUEZ };