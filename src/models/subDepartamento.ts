import { ASEMPUEZ } from "../db/db";
import * as bookshelf from "bookshelf";

export class subDepartamento extends ASEMPUEZ.Model<subDepartamento> {

  get tableName() { return 'astipocedu'; }

}

export interface subDepartamento extends bookshelf.Model<subDepartamento> {
  ccodsubdep: string
  cnomsubdep: string
  ctelefono: string
  cextension: number
}
