import { Prestamo, PrestamoInterface } from "./model";

const { gql } = require('apollo-server');

const typeDefs = gql`
    
  type Prestamo {
    montoAprobado: Float
    fechaForm: Date
    periodoCierre: Date
    numSolicitud: String
    cuotaPrestamo: Float
    cuotas: Float
    saldoCredito: Float
  }

`;

const resolvers = {
  Prestamo: {
    montoAprobado: ({ nmontoapro }) => nmontoapro,
    fechaForm: ({ dfechaform }) => dfechaform,
    periodoCierre: ({ dpercierre }) => dpercierre,
    numSolicitud: ({ cnumsolici }) => cnumsolici,
    cuotaPrestamo: ({ ncuotapres }) => ncuotapres,
    cuotas: ({ nnumcuotas }) => nnumcuotas,
    saldoCredito: ({ nsaldocred }) => nsaldocred,
  }
};

export { resolvers as PrestamoResolvers, typeDefs as PrestamoDefs }