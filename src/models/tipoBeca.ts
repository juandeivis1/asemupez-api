import { ASEMPUEZ } from "../db/db";
import * as bookshelf from "bookshelf";

export class tipoBeca extends ASEMPUEZ.Model<tipoBeca> {

  get tableName() { return 'astipocedu'; }

}

export interface tipoBeca extends bookshelf.Model<tipoBeca> {
  ccodibecas: number,
  cdescripci: string,
  ccuentacon: string,
  nmontobeca: number
}
