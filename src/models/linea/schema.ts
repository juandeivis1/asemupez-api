const { gql } = require('apollo-server');

const typeDefs = gql`
    
  type Linea {
    codigo: Int
    detalle: String
  }

`;

const resolvers = {
  Linea: {
    codigo: ({ ccodigolin }) => ccodigolin,
    detalle: ({ cdetalleli }) => cdetalleli.trim(),
  }
};

export { resolvers as LineaResolvers, typeDefs as LineaDefs }