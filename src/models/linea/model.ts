import { ASEMPUEZ } from "../../db/db";
import * as bookshelf from "bookshelf";

export class Linea extends ASEMPUEZ.Model<LineaInterface> {

  get tableName() { return 'crcodlinea'; }

}

export interface LineaInterface extends Linea {
  ccodigolin: String
  cdetalleli: String
}