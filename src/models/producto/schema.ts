import { Deduccion, DeduccionInterface } from '../deduccion/model'
import { Movimiento, MovimientoInterface } from '../movimiento/model';
const { gql } = require('apollo-server');

const typeDefs = gql`
    
  type Producto {
    monto: Float
    codigo: Deduccion
    porcentaje: Float
    cuota: Float
    interes: Float
    pagos: Float
    movimiento: Date
  }  
`;

const resolvers = {
  Producto: {
    monto: ({ nmtoprinci }) => nmtoprinci,
    codigo: ({ ccoddeducc }) => Deduccion.where<DeduccionInterface>({ ccoddeducc }).fetch().call('toJSON'),
    porcentaje: ({ nmtoporcen }) => nmtoporcen,
    cuota: ({ nmtocuotas }) => nmtocuotas,
    interes: ({ nmtointere }) => nmtointere,
    pagos: ({ cnumepagos }) => cnumepagos,
    movimiento: ({ dfepagreal }) => dfepagreal,
  },

};

export { resolvers as ProductoResolvers, typeDefs as ProductoDefs }

