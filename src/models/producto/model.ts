import { ASEMPUEZ } from "../../db/db";
import * as bookshelf from "bookshelf";

export class Producto extends ASEMPUEZ.Model<Producto> {

  get tableName() { return 'dededucaso'; }

}

export interface ProductoInterface extends bookshelf.Model<ProductoInterface> {
    cidasociad: number
    ccoddeducc: string
    cnumdeducc: string
    nmtoporcen: string
    nmtocuotas: string
    nmtoprinci: number
    nmtointere: string
    nmtopendie: string
    nmtotransi: string
    ncuotatran: string
    ncargoactu: string
    ncargopend: string
    nplazodedu: string
    cnumepagos: string
    ccuentacon: string
    cnombusuar: string
    dfechainic: string
    dfechafina: string
    dfecactman: string
    dfecactpla: string
    dfecultenv: string
    cnombalias: string
    ccuentaint: string
    dfecultcap: string
    cprodconge: string
    dfechconge: string
    dfechdesco: string
    cdescripci: string
    cpagocuota: string
    ccodigoges: string
    cnumoperac: string
    ctipoconge: string
    dfechlimit: string
    cpagocomis: string
    nultsalcal: string
    dfepagreal: string
    ccodibanco: string
    cnumtarjet: string
    cnumpoliza: string
}