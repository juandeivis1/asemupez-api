import { ASEMPUEZ } from "../../db/db";
import * as bookshelf from "bookshelf";

export class MovimientoCredito extends ASEMPUEZ.Model<MovimientoCredito> {

  get tableName() { return 'crregitran'; }

}

export interface MovimientoCreditoInterface extends bookshelf.Model<MovimientoCreditoInterface> {
  cidasociad: number
  ccoddeducc: number
  cnumdeducc: string
  nmontomovi: string
  nintermovi: string
  nmtocargos: string
  ncuoadmisi: string
  nnumconsec: string
  dfechamovi: string
  ctipomovim: string
  cstatusmov: string
  casientafe: string
  dfechaasie: string
  cusuarmovi: string
  cnumpagos: string
  nsaldprinc: string
  nsaldinter: string
  corigmovim: string
  ncomisinte: string
  cmovianula: string
  ctipasient: string
  cnumasient: string
  cnumrecibo: string
  cctabancar: string
  nnumdocume: string
  ctipodocum: string
  dfechacort: string
  cnumoperac: string
  prdescripc: string
  nporctasa: string
}