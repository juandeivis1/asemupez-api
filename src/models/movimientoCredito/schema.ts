import { MovimientoCredito, MovimientoCreditoInterface } from "./model";

const { gql } = require('apollo-server');

const typeDefs = gql`
    type MovimientoCredito {
    id: Int
    montoMovimiento: Float
    fechaMovimiento: Date
    numPago: Float
    montoPrincipal: Float
  }

`;

const resolvers = {
  MovimientoCredito: {
    id: ({ cnumrecibo }) => cnumrecibo,
    fechaMovimiento: ({ dfetransac }) => dfetransac,
    numPago: ({ nnumecuota }) => nnumecuota,
    montoMovimiento: ({ nmontotran }) => nmontotran,
    montoPrincipal: ({ nsaldoactu }) => nsaldoactu,
  },
  Query: {
    movimientosCredito: async (obj, { codigo }, { socio }) => {      
      return await MovimientoCredito.collection<MovimientoCreditoInterface>()
        .query(qb => {
          qb.where({ cnumoperac: codigo })
            .orderBy('dfetransac', 'desc')
            .limit(12)
        })
        .fetch().call('toJSON');
    }
  }
};

export { resolvers as MovimientoCreditoResolvers, typeDefs as MovimientoCreditoDefs }