import { ASEMPUEZ } from "../../db/db";
import * as bookshelf from "bookshelf";

export class Credito extends ASEMPUEZ.Model<CreditoInterface> {

  get tableName() { return 'crprestamo'; }

}

export interface CreditoInterface extends Credito {
  cnumoperac: string
  cnumsolici: string
  cidasociad: number
  dfechaform: Date
  nmontoapro: number
  nnumcuotas: number
  ntasainter: number
  nmontocarg: number
  npagosefec: number
  nsaldocred: number
  dfeculabon: Date
  nDesembols: number
  ncuotapres: number
  ccodigolin: string
}