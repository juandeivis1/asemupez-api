import { Linea, LineaInterface } from "../linea/model";

const { gql } = require('apollo-server');

const typeDefs = gql`
    
  type Credito {
    id: String
    solicitud: String
    formalizado: Date
    montoAprovado: Float
    cuota: Float
    cuotas: Float
    tasaInteres: Int
    cargos: Float
    pagos: Int
    saldo: Float
    ultimo: Date
    desembolsado: Float
    tipo: Linea
  }
`;

const resolvers = {
  Credito: {
    id: ({ cnumoperac }) => cnumoperac,
    solicitud: ({ cnumsolici }) => cnumsolici,
    formalizado: ({ dfechaform }) => dfechaform,
    montoAprovado: ({ nmontoapro }) => nmontoapro,
    cuota: ({ ncuotapres }) => ncuotapres,
    cuotas: ({ nnumcuotas }) => nnumcuotas,
    tasaInteres: ({ ntasainter }) => ntasainter,
    cargos: ({ nmontocarg }) => nmontocarg,
    pagos: ({ npagosefec }) => npagosefec,
    saldo: ({ nsaldocred }) => nsaldocred,
    ultimo: ({ dfeculabon }) => dfeculabon,
    desembolsado: ({ nDesembols }) => nDesembols,
    tipo: ({ ccodigolin }) => Linea.where<LineaInterface>({ ccodigolin }).fetch().call('toJSON')
  }
};

export { resolvers as CreditoResolvers, typeDefs as CreditoDefs }