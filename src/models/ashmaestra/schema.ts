import { User, UserInterface } from "./model";
import { Prestamo, PrestamoInterface } from "../prestam/model";

const { gql } = require('apollo-server');

const typeDefs = gql`
  scalar Date
    
  type User {
    id: Int
    name: String
    identification: String
    address: String
    birth: Date
    prestamos: [Prestamo]
  }

  type Query {
    user(id: Int): User!
  }

`;

const resolvers = {
  User: {
    id: ({ cidasociad }) => cidasociad,
    name: ({ cnombrecom }) => cnombrecom,
    identification: ({ ccedulasoc }) => ccedulasoc.trim(),
    address: ({ cdireccaso }) => cdireccaso.trim(),
    birth: ({ dfechanaci }) => dfechanaci,
    prestamos: async ({ cidasociad }) => Prestamo.where<PrestamoInterface>({ cidasociad }).fetchAll().call('toJSON'),
  },
  Query: {
    user: async (parent, args, context) => {      
      return await User.where<UserInterface>({ cidasociad: args.id }).fetch().call('toJSON')
    },
  },
};

export { resolvers as UserResolvers, typeDefs as UserDefs }