const { gql } = require('apollo-server');

const typeDefs = gql`
    
  type Deduccion {
    id: Int
    descripcion: String
  }

#   type Query {
#     user(id: Int): User!
#   }

`;

const resolvers = {
  Deduccion: {
    id: ({ ccoddeducc }) => ccoddeducc,
    descripcion: ({ cdesdeducc }) => cdesdeducc.trim(),
  },
  // Query: {
  //     // user: async (parent, args, context) => {
  //     //   console.log(args);

  //     //   return await User.where<UserInterface>({ cidasociad: args.id }).fetch().call('toJSON')
  //     // },
  // },
};

export { resolvers as DeduccionResolvers, typeDefs as DeduccionDefs }