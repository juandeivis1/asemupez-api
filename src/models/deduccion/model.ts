import { ASEMPUEZ } from "../../db/db";
import * as bookshelf from "bookshelf";

export class Deduccion extends ASEMPUEZ.Model<Deduccion> {

  get tableName() { return 'decdeducci'; }

}

export interface DeduccionInterface extends bookshelf.Model<DeduccionInterface> {
  ccoddeducc: number
  cdesdeducc: string
}