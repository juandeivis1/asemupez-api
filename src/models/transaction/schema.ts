import { Transaction, TransactionInterface } from "./model";
import { Concepto, ConceptoInterface } from "../concepto/model";
const { gql } = require('apollo-server');

const typeDefs = gql`
    
  type Transaction {
    id: String
    fechaTrans: Date
    concepto: String
    montoTrans: Float
    montoPrincipal: Float
  }
`;

const resolvers = {
  Transaction: {
    id: ({ cnumoperac }) => cnumoperac.trim(),
    fechaTrans: ({ dfetransac }) => dfetransac,
    concepto: async ({ ccodconcep }) => {
      const concepto = await Concepto.where<ConceptoInterface>({ ccodconcep }).query(qb => qb.select('cdetaconce')).fetch().call('toJSON')
      return concepto.cdetaconce;
    } ,
    montoTrans: ({ nmontotran }) => nmontotran,
    montoPrincipal: ({ namorprinc }) => namorprinc,
  },
};

export { resolvers as TransactionResolvers, typeDefs as TransactionDefs }