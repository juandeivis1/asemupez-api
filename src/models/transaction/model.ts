import { ASEMPUEZ } from "../../db/db";
import * as bookshelf from "bookshelf";

export class Transaction extends ASEMPUEZ.Model<Transaction> {

  get tableName() { return 'crregitran'; }

}

export interface TransactionInterface extends bookshelf.Model<TransactionInterface> {
  cidasociad: Number
  cnumoperac: String
  ccodtipabo: String
  ccodconcep: Number
  dfetransac: Date
  cnumrecibo: String
  nmontotran: Number
  namorprinc: String
  ninterescor: String
  ninteresmo: String
  nnumecuota: String
  nmontotros: String
  nmonpoliza: String
  corigmovim: String
  ccodigolin: String
  nsaldoactu: String
  cmovianula: String
  nnumconsec: String
  nintcormor: String
  ccodigousu: String
  cnumasient: String
  ctipasient: String
  cobserva01: String
  cobserva02: String
  ncuotcance: String
  nintnocobr: String
  dfeproxabo: String
  dfecultabo: String
  dfepagreal: String
  dfechacort: String
  cnumoprefu: String
  ccodgestor: String
  ndiasatras: String
  ntasainter: String
  cConPlaTra: String
}