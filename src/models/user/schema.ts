import * as admin from "firebase-admin";
import { User, UserInterface } from "./model";
import { Transaction, TransactionInterface } from "../transaction/model";
import { Producto, ProductoInterface } from "../producto/model";
import { Credito, CreditoInterface } from "../credito/model";
const { gql } = require('apollo-server');
import * as nodemailer from 'nodemailer';

const typeDefs = gql`
  scalar Date
    
  type User {
    id: Int
    name: String
    identification: String
    address: String
    birth: Date
    transactiones: [Transaction]
    productos: [Producto]
    creditos: [Credito]
  }

  type Query {
    user: User
    getMovimientos(codigo: Int): [Movimiento]
    movimientosCredito(codigo: String): [MovimientoCredito]
    buscarSocio(email: String) : AuthUser
    registrados: [FirebaseUser]
  }

  type Mutation {
    definirSocio(uid: String, socio: Int) : AuthUser
    registrarTramite(id: Int, name: String): Boolean 
  }

  type AuthUser {
    uid: String
    email: String
    displayName: String
    photoURL: String
    socio: User
  }

  type FirebaseUser {
    uid: String
    email: String
    displayName: String
    photoURL: String
    socio: Int
  }

`;

const resolvers = {
  User: {
    id: ({ cidasociad }) => cidasociad,
    name: ({ cnombrecom }) => cnombrecom,
    identification: ({ ccedulasoc }) => ccedulasoc.trim(),
    address: ({ cdireccaso }) => cdireccaso.trim(),
    birth: ({ dfechanaci }) => dfechanaci,
    transactiones: ({ cidasociad }) => Transaction.where<TransactionInterface>({ cidasociad }).fetchAll().call('toJSON'),
    productos: async ({ cidasociad }) => await Producto.where<ProductoInterface>({ cidasociad }).where('ccoddeducc', '!=', 16).where('ccoddeducc', '!=', 6).where('ccoddeducc', '!=', 5).fetchAll().call('toJSON'),
    creditos: async ({ cidasociad }) => Credito.where<CreditoInterface>({ cidasociad }).where('nsaldocred','>',0).fetchAll().call('toJSON')
  },
  AuthUser: {
    uid: ({ uid }) => uid,
    email: ({ email }) => email,
    displayName: ({ displayName }) => displayName,
    photoURL: ({ photoURL }) => photoURL,
    socio: async ({ customClaims }) => User.where<UserInterface>({ cidasociad: customClaims.socio }).fetch().call('toJSON') || null,
  },
  Query: {
    user: async (parent, args, context) => {

      if (!context.socio) {
        return null
      }
      return await User.where<UserInterface>({ cidasociad: context.socio }).fetch().call('toJSON')
    },
    buscarSocio: async (parent, args, context) => {
      try {
        return await admin.auth().getUserByEmail(args.email)
      } catch (err) {
        return null;
      }
    },
    registrados: async () => {
      const { users } = await admin.auth().listUsers(500)
      let usersOrdered = users.sort((a,b) => new Date (a.tokensValidAfterTime).getTime() - new Date (b.tokensValidAfterTime).getTime());
      usersOrdered = usersOrdered.map(x => {        
        if (x.customClaims) {
          return { ...x, socio: x.customClaims['socio']}
        }
        return x
      })
      return usersOrdered;
    }
  },
  Mutation: {
    definirSocio: async (parent, args, context) => {
      const { uid, socio } = args;
      await admin.auth().setCustomUserClaims(uid, { socio });
      return await admin.auth().getUser(uid);
    },
    registrarTramite: (_, args) => {

      var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
               user: 'juandeivis1@gmail.com',
               pass: 'bbkkasmuogtnmjzd'
           }
       });

       const mailOptions = {
        from: 'asemupez@gmail.com', // asemupez@gmail.com
        to: 'juandeivis1@gmail.com', 
        subject: `Nuevo tramite registrado socio: ${args.id}`,
        html: `<p>${args.name} ha registrado un nuevo tramite.</p>`
      };

      transporter.sendMail(mailOptions,  (err, info) => {
        if(err)
          console.log(err)
     });
    }
  }
};

export { resolvers as UserResolvers, typeDefs as UserDefs }