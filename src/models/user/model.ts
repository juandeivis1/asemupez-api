import { ASEMPUEZ } from "../../db/db";
import * as bookshelf from "bookshelf";

export class User extends ASEMPUEZ.Model<User> {

  get tableName() { return 'asmaestras'; }

}

export interface UserInterface extends bookshelf.Model<UserInterface> {
  cidasociad: string
  ccedulasoc: string
  cclaveloca: string
  ctipocedul: string
  cnombreaso: string
  cconoccomo: string
  cnacionaso: string
  cinstuasoc: string
  cidivision: string
  cinstdepto: string
  cdireccaso: string
  cteletraba: string
  cextentrab: string
  cteledomic: string
  ctelecelul: string
  cemailasoc: string
  cvehicuaso: string
  cotrasinst: string
  cctasbanca: string
  cprofesaso: string
  ccondlabor: string
  ccondasoci: string
  cformapago: string
  ctipoplani: string
  csexoasoci: string
  dfechanaci: Date
  dfechaingc: Date
  dfechainga: Date
  dfechasali: Date
  cestadociv: string
  ccondelega: string
  cformenvio: string
  nsalarioas: number
  nsalarione: number
  ccodpuesto: string
  ccodigocat: string
  dfecharenu: Date
  ccodigoca2: string
  ccodgrupos: string
  cestasocio: string
  ccodigomot: string
  cComentari: string
  nAfiliaNum: number
  cusucreado: string
  cusuamodif: string
  dfechacrea: Date
  dfechamofi: Date
  cnombrecom: string
  cfirmletra: string
  ccodareate: string
  ccodsubdep: string
  cdiretraba: string
  ncanthijos: number
  cmuestclav: string
  caccesoweb: string
  ncontplato: number
  nmtodispex: number
  cexoneraiv: string
  cperwebpre: string
  ctipingres: string
  nAnoServic: number
  cTipHabita: string
  nCanDepend: number
  cfirmlesms: string
  cenviadedu: string
  nSalSegQui: number
  ccodpaisor: string
  cemailaso2: string
  nrifaasamb: number
  cconttecno: string
  nporcliqui: number
  cciercredi: string
  ndeducccss: number
  nahorroemp: number
  ndeudasemp: number
  ndeubanpop: number
  ncuotasoen: number
  nmontoemba: number
  cvaliembar: string
  ncuotaemba: number
  cvacuoesca: string
  cnumpoliza: string
  capellido1: string
  capellido2: string
  cnombasoci: string
  cidvendedo: string
}