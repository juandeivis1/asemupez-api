import { Movimiento, MovimientoInterface } from "./model";

const { gql } = require('apollo-server');

const typeDefs = gql`
    
  type Movimiento {
    id: Int
    fechaMovimiento: Date
    numPago: Int
    montoMovimiento: Float
    montoPrincipal: Float
  }
`;

const resolvers = {
  Movimiento: {
    id: ({ nnumconsec }) => nnumconsec,
    fechaMovimiento: ({ dfechamovi }) => dfechamovi,
    numPago: ({ cnumpagos }) => cnumpagos,
    montoMovimiento: ({ nmontomovi }) => nmontomovi,
    montoPrincipal: ({ nsaldprinc }) => nsaldprinc,
  },
  Query: {
    getMovimientos: async (obj, { codigo, off }, { socio }) => {      
      return await Movimiento.collection<MovimientoInterface>()
        .query(qb => {
          qb.where({ ccoddeducc: codigo })
            .andWhere({ cidasociad: socio })
            .orderBy('dfechacort', 'desc')
            .limit(12)
        })
        .fetch().call('toJSON');
    }
  }
};

export { resolvers as MovimientoResolvers, typeDefs as MovimientoDefs }