import { ASEMPUEZ } from "../db/db";
import * as bookshelf from "bookshelf";

export class tipoCedula extends ASEMPUEZ.Model<tipoCedula> {

  get tableName() { return 'astipocedu'; }

}

export interface tipoCedula extends bookshelf.Model<tipoCedula> {
  ctipocedul: number,
  cdescrcedu: string
}