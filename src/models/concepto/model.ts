import { ASEMPUEZ } from "../../db/db";
import * as bookshelf from "bookshelf";

export class Concepto extends ASEMPUEZ.Model<ConceptoInterface> {

  get tableName() { return 'crconcepto'; }

}

export interface ConceptoInterface extends Concepto {
  ccodconcep: String
  cdetaconce: String
}