import * as express from 'express';
import * as admin from 'firebase-admin';
import { userByEmail } from './ninja';
const router = express.Router()

router.use((req, res, next) => {
    const { authorization } = req.headers;
    const auth = admin.auth();
    auth.verifyIdToken(authorization).then(({ninja}) => {
        
       !!ninja ? next() : res.status(500).send('Not Ninja') ;
    })
  })

  router.post('/admin', (req, res)=> {
    // res.json({})
    res.json({ninja: true});
  }) 

  router.post('/byEmail', userByEmail)

export { router as ninjaRouter };