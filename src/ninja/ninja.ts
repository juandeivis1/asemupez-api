import * as admin from 'firebase-admin';

export const makeAdmin = () => {

}

export const userByEmail = (req, res)=> {
    const { email } = req.body;
    const auth = admin.auth();
    return auth
        .getUserByEmail(email)
        .then(({ uid, displayName, email, photoURL, customClaims }) => {
            res.json({uid, displayName, email, photoURL, customClaims})
        })
        .catch(err=> {
            res.status(500).send('No existe usuario!');
        })
}

export const userByUid = (req, res)=> {
    const { uid } = req.body;
    const auth = admin.auth();
    return auth
        .getUser(uid)
        .then(({ uid, displayName, email, photoURL, customClaims }) => {
            res.json({uid, displayName, email, photoURL, customClaims})
        })
        .catch(err=> {
            res.status(500).send('No existe usuario!');
        })
}